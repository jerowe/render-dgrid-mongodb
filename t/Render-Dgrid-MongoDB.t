# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl Render-Dgrid-MongoDB.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

package MyTable;
use strict;
use warnings;
use Data::Dumper;

use Test::More tests => 4;
BEGIN { use_ok('MongoDB') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

BEGIN { push @INC, '/home/jillian/perlmodules/Render/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-Dgrid-MongoDB/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-Dgrid/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-MongoDB/lib' };

use Moose;
extends 'Render';
with 'Render::MongoDB';
with 'Render::Dgrid::MongoDB';

my $dt = MyTable->new();
my $cl;

my $client = MongoDB::MongoClient->new(host => 'jilldev.qatar-med.cornell.edu:27017');
my $db = $client->get_database( 'test1' );
my $col = $db->get_collection( 'test2' );
$dt->mongodb_db($db);
$dt->mongodb_collection($col);

my $aref = [ {a => 'apple', b=> 'boat', POS => 1}, {a => 'a', b => 'b', POS => 2}, {a => 'adam', b=>'bell', POS => 3} ];
foreach my $row (@$aref) {
    ok($cl = $dt->mongodb_collection->insert($row), 'inserted correctedly');
}
$col->get_collection('column_names')->insert({name => ['a', 'b', 'POS']});

my($start, $end) = (0, 10);

my $pagesize = $end-$start+1;

$col->ensure_index( Tie::IxHash->new( "POS" => 1 ) );
my $count = $col->find->count;
my $all_records = $col->find->skip($start)->limit($pagesize)->fields({POS => 1});
$dt->columns(["POS"]);
$dt->stream($all_records);
$dt->process();

my $tmp;
ok($tmp = $dt->dgdata, 'got dgdata fine');

foreach my $t (@$tmp){ delete $t->{'_id'}; }
is_deeply($tmp, $aref, "Objects match!");

$tmp =  {
    'columns' => {
    'a' => 'a',
    'b' => 'b',
    'POS' => 'POS'
    }
};

diag Dumper($dt->dgcolumn);
is_deeply($tmp, $dt->dgcolumn, 'The columns match');

diag Dumper($dt->dgdata);
$dt->mongodb_db->drop;

done_testing();
