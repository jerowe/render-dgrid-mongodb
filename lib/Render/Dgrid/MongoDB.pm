package Render::Dgrid::MongoDB;

use 5.010000;
use strict;
use warnings;

require Exporter;
use Carp;
use Class::MOP;
use Data::Dumper;
use Tie::IxHash;
use Moose::Role;

with 'Render::Dgrid';

our @ISA = qw(Exporter);

our $VERSION = '0.01';

has 'stream' => (
    is => 'rw',
    isa => 'MongoDB::Cursor',
);

sub render_rows_as_hashref{
    my $self = shift;

    croak "Didn't pass a stream object" unless $self->stream;

    $self->dgdata([]);
    $self->dgid('_id');

    while ( my $block = $self->stream->next ) {
        if(ref($block) eq "HASH"){
            #MongoDB ID
            if(exists $block->{_id}){
                $block->{_id} = $block->{_id}->{value};
            }
            elsif(${\( $self->dgid )} eq "_id"){
                my $id = join'', map +(0..9,'a'..'z','A'..'Z')[rand(10+26*2)], 1..15;
                $block->{_id} = $id;
            }
            push(@{$self->dgdata}, $block);
        } 
        else{
#            croak "Stream ref is not a hash! Dying!";
        }
    }
}

sub render_header_as_hashref{
    my $self = shift;
    my(%href, @cols);
    tie %href, 'Tie::IxHash';

    @cols = @{$self->get_column_names};
    carp "There are no columns in Dgrid!" unless @cols;

    #Check for length!

    if( $self->display_columns &&  (@{$self->display_columns} == @cols)){
        $self->display_columns(\@cols);
    }
    else{
        $self->display_columns(\@cols);
    }
    @href{@cols} = @{$self->display_columns};

    $self->dgcolumn({});
    $self->dgcolumn->{columns} = \%href;
}

sub get_column_names{
    my $self = shift;
    my($cols);

    #If creating the object directly from TextCSV_XS just return the column names
    return [$self->textcsv_xs->column_names()] if $self->{textcsv_xs};
    #Make sure it exists and isn't just an empty array
    return $self->columns if @{$self->columns} > 0;

    croak "There is no mongodb_collection specified! " unless $self->mongodb_collection;
    my $collection = $self->mongodb_collection;
    my $column_records = $collection->get_collection('column_names')->find;
    while (my $doc = $column_records->next) {
        push(@$cols, @{$doc->{'name'}});
    }


    croak "There are no columns! " unless $cols;

    return $cols;
}

use namespace::autoclean;
1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Render::Dgrid::MongoDB - Perl extension for blah blah blah

=head1 SYNOPSIS

  use Render::Dgrid::MongoDB;

=head1 DESCRIPTION

Stub documentation for Render::Dgrid::MongoDB, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.

=head1 Tags

=head2 resultset

Required parameter, isa DBIx::Class::ResultSet. All querying should be done in the controller to give the renderer the exact resultset required.

=head2 process

Sets up the dgrid table data and columns from the resultset

=head2 dgcolumn

Column ref portion of dgrid
Perl like this

    my $columns = {columns => {first => "First Name", last => "Last Name", age => "Age"}};

Json like this

    columns: {
        first: "First Name",
        last: "Last Name",
        age: "Age"
    }

=head2 dgdata

Data ref portion of dgrid
Perl hash like this

    my $data = [ {first => "Bob", last => "Barker", age => 89}, {first => "Vanna", last => "White", age => 55}, {first => "Pat", last => "Sajak", age => 65} ];

Json hash like this

    var data = [ 
        { first: "Bob", last: "Barker", age: 89 },
        { first: "Vanna", last: "White", age: 55 },
        { first: "Pat", last: "Sajak", age: 65 }
    ];

=head2 pretty_json

Boolean value to print pretty json or regular json

=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Jillian Rowe, E<lt>jillian@localdomainE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
